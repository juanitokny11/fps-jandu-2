using UnityEngine;
using System.Collections;

public class BallShoot : MonoBehaviour
{

    public GameObject bullet_prefab;
    public GameObject rocket_prefab;
    float bulletImpulse = 20f;
    float rocketForce = 1000f;
    public Transform disparo;
    public Transform cohete;

    // Update is called once per frame
    public void Shot()
    {
        GameObject thebullet = (GameObject)Instantiate(bullet_prefab, disparo.transform.position + Camera.main.transform.forward, Camera.main.transform.rotation);
        thebullet.GetComponent<Rigidbody>().AddForce(Camera.main.transform.forward * bulletImpulse, ForceMode.Impulse);
    }
    public void Shot_rocket()
    {
        GameObject thebullet = (GameObject)Instantiate(rocket_prefab, cohete.transform.position + Camera.main.transform.forward, Camera.main.transform.rotation);
        thebullet.GetComponent<Rigidbody>().AddForce(Camera.main.transform.forward * rocketForce, ForceMode.Force);
    }
   
}
