using UnityEngine;
using System.Collections;

public class BallDetonator : MonoBehaviour {
	
	float lifespan = 3.0f;
	public GameObject fireEffect;
	
	// Update is called once per frame
	void Update () {
		lifespan -= Time.deltaTime;
		
		if(lifespan <= 0) {
			Explode();
		}
	}
	
	void OnCollisionEnter(Collision collision) {
		
		if(collision.gameObject.tag == "Enemy" || collision.gameObject.tag == "Suelo"|| collision.gameObject.tag == "Passive") {
			//collision.gameObject.tag = "Untagged";
			GameObject fire = Instantiate(fireEffect, collision.transform.position, Quaternion.identity);
			fire.AddComponent<RemoveExplosion> ();
			Explode();		
		}
	}
	
	void Explode() {
		
		Destroy(gameObject);
	}
}
