﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemGenerator : MonoBehaviour {

	public GameObject ammo;
    public GameObject botiquin;
    public GameObject rockets;
	public float timeToSpawn;
    private GameObject icreeper;
    private Vector3 random;
    private int num;


    private void Update()
    {
        random.x = Random.Range(7, 31);
        random.y = Random.Range(0,12);
        random.z = Random.Range(-18, 18);

        ammo.transform.position = random;
        botiquin.transform.position = random;
       rockets.transform.position = random;
        num = Random.Range(1, 3);
    }
    IEnumerator Start(){
		while (true) {
			yield return new WaitForSeconds (timeToSpawn);
            if (num == 1) {
                 icreeper = Instantiate(ammo);
            }else if (num == 2)
            {
                icreeper = Instantiate(botiquin);
            }
            else if (num == 3)
            {
                icreeper = Instantiate(rockets);
            }
		}
	}
}
