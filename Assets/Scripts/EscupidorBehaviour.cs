﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EscupidorBehaviour : MonoBehaviour
{
    public enum State { Idle, Patrol, Chase, Attack, Dead };
    public State state;

    private NavMeshAgent agent;
    private Animator anim;
    public SoundPlayer sound;
    public Transform playerpos;
    public Vector3 playerposV;
    // private BoxCollider attackcollider;
    public GameObject bullet_prefab;
    float bulletImpulse = 20f;
    public Transform disparo;
    public bool ataque = false;
    private Quaternion lookRotation;
    public FPSInputManager manager;

    [Header("Creeper properties")]
    public int life = 7;

    [Header("Target Detection")]
    public float radius;
    public float idleRadius;
    public float chaseRadius;
    public LayerMask targetMask;
    public bool targetDetected = false;
    private Transform targetTransform;
    private float rotationSpeed=10f;

    [Header("Patrol path")]
    public bool stopAtEachNode = true;
    public float timeStopped = 1.0f;
    private float timeCounter = 0.0f;
    public Transform[] pathNodes;
    private int currentNode = 0;
    private bool nearNode = false;

    [Header("Explosion properties")]
    public float explodeDistance;
    public float explosionRadius;
    public float explosionForce;
    //public ParticleSystem explosionPS;

    // Use this for initialization
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        anim = GetComponentInChildren<Animator>();
        //attackcollider = GetComponent<BoxCollider>();
        // sound = GetComponentInChildren<SoundPlayer>();

        nearNode = true;
        SetIdle();
    }

    // Update is called once per frame
    void Update()
    {
        switch (state)
        {
            case State.Idle:
                Idle();
                break;
            case State.Patrol:
                Patrol();
                break;
            case State.Chase:
                Chase();
                break;
            case State.Attack:
                Explode();
                break;
            case State.Dead:
                Dead();
                break;
            default:
                break;
                
        }
    }
    private void FixedUpdate()
    {
        targetDetected = false;
        Collider[] cols = Physics.OverlapSphere(this.transform.position, radius, targetMask);
        if (cols.Length != 0)
        {
            targetDetected = true;
            targetTransform = cols[0].transform;
        }
    }

    void Idle()
    {
        if (targetDetected)
        {
            SetChase();
            return;
        }

        if (timeCounter >= timeStopped)
        {
            if (nearNode) GoNearNode();
            else GoNextNode();
            SetPatrol();
        }
        else timeCounter += Time.deltaTime;
    }
    void Patrol()
    {
        if (targetDetected)
        {
            SetChase();
            return;
        }

        if (agent.remainingDistance <= 0.1f)
        {
            if (stopAtEachNode) SetIdle();
            else GoNextNode();
        }
    }
    void Chase()
    {
        if (!targetDetected)
        {
            nearNode = true;
            SetIdle();
            return;
        }
        agent.SetDestination(targetTransform.position);

        if (Vector3.Distance(transform.position, targetTransform.position) <= explodeDistance)
        {
            SetExplode();
        }
      

    }
    void Explode() { }
    void Dead() { }

    void SetIdle()
    {
        agent.isStopped = true;
        anim.SetBool("Walk", false);
        anim.SetBool("Run", false);
        radius = idleRadius;
        timeCounter = 0;
        if (manager.pause == true)
        {
            sound.Play(0, 1);
        }

        state = State.Idle;
    }
    void SetPatrol()
    {
        agent.isStopped = false;
        agent.stoppingDistance = 0;
        radius = idleRadius;
        anim.SetBool("Walk", true);
        anim.SetBool("Run", false);

        state = State.Patrol;
    }
    void SetChase()
    {
        agent.isStopped = false;
        agent.SetDestination(targetTransform.position);
        agent.stoppingDistance = 2.0f;
        anim.SetBool("Run", true);
        anim.SetBool("Walk", false);
        radius = chaseRadius;

        state = State.Chase;
    }
    void SetExplode()
    {
        agent.updateRotation = true;
        transform.rotation = Quaternion.RotateTowards(transform.rotation, lookRotation, Time.deltaTime * rotationSpeed);
        RotateTowards(playerpos);
        agent.isStopped = true;
        transform.tag = "Enemy";
        anim.SetBool("Attack", true);
        if (ataque == true) {
            Shoot_slime();
             if (manager.pause == true)
            {

                sound.Play(2, 1);
            }
        }
        ataque = false;
        Invoke("ResetAttack", 3);
        state = State.Attack;
    }
    void SetDead()
    {
        agent.isStopped = true;
        state = State.Dead;
        anim.SetTrigger("Die");
        ataque = false;
        if (manager.pause == true)
        {
            sound.Play(1, 1);
        }
        Invoke("DestroyEnemy", 2);
    }

    void GoNextNode()
    {
        currentNode++;
        if (currentNode >= pathNodes.Length) currentNode = 0;

        agent.SetDestination(pathNodes[currentNode].position);
    }
    void GoNearNode()
    {
        nearNode = false;
        float minDistance = Mathf.Infinity;
        for (int i = 0; i < pathNodes.Length; i++)
        {
            if (Vector3.Distance(transform.position, pathNodes[i].position) < minDistance)
            {
                minDistance = Vector3.Distance(transform.position, pathNodes[i].position);
                currentNode = i;
            }
        }
        agent.SetDestination(pathNodes[currentNode].position);
    }

    public void Explosion()
    {
        Collider[] cols = Physics.OverlapSphere(this.transform.position, explosionRadius);

        foreach (Collider c in cols)
        {
            if (c.attachedRigidbody != null)
            {
                //c.attachedRigidbody.AddExplosionForce(explosionForce, this.transform.position, explosionRadius, 1, ForceMode.Impulse);
            }
        }
        // int random = Random.Range(4, 8);
        // sound.Play(random, 1);
        // explosionPS.Play();
        // explosionPS.transform.parent = null

        
        //SetDead();
    }
    private void OnDrawGizmos()
    {
        Color color = Color.green;
        if (targetDetected) color = Color.red;
        color.a = 0.1f;
        Gizmos.color = color;
        Gizmos.DrawSphere(this.transform.position, radius);
    }

    public void Damage(int hit)
    {
        if (state == State.Dead) return;
        life -= hit;
        if (life <= 0) SetDead();
    }

    void OnCollisionEnter(Collision other)
    {

        if (other.gameObject.tag == "Shot")
        {
            Damage(2);
        }
        if (other.gameObject.tag == "Misile")
        {
            Damage(5);
        }
    }
    void ResetAttack()
    {
        anim.SetBool("Attack", false);
        agent.isStopped = false;
        ataque = true;
        transform.tag = "Passive";
        state = State.Patrol;
    }
    void DestroyEnemy()
    {
        Destroy(this.gameObject);
    }
    public void Shoot_slime()
    {
        if (ataque == true) { 
        GameObject thebullet = (GameObject)Instantiate(bullet_prefab, disparo.transform.position, disparo.transform.rotation);
        thebullet.GetComponent<Rigidbody>().AddForce(disparo.transform.forward * bulletImpulse, ForceMode.Impulse);
        }
    }
    private void RotateTowards(Transform target)
    {
        Vector3 direction = (target.position - transform.position).normalized;
        lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));    // flattens the vector3
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * rotationSpeed);
    }
}
