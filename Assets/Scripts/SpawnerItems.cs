﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerItems : MonoBehaviour {
    public GameObject[] Enemies;
    public GameObject[] spawn;
    public float timeToSpawn;
    public float maxTimeSpawn;
    public float minTimeSpawn;
    public FPSInputManager manager;
    public float startWait;
    private GameObject randomTarget;
    public bool stop;
    int randenemy;
    private void Awake()
    {
        spawn = GameObject.FindGameObjectsWithTag("Target2");
    }
    void Update () {
        timeToSpawn = Random.Range(maxTimeSpawn, minTimeSpawn);
        
    }
    IEnumerator Start()
    {
            yield return new WaitForSeconds(startWait);
        while (!stop)
        {
            randenemy = Random.Range(0, 2);
            randomTarget = spawn[Random.Range(0, spawn.Length)];
            if ( manager== true)
            {
                Instantiate(Enemies[randenemy], randomTarget.transform.position , gameObject.transform.rotation);
            }
           
            yield return new WaitForSeconds(timeToSpawn);
        }
    }
}
